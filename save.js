(function() {
  function saveFile(data, filename, type) {
    var file = new Blob([data], {type: type});
    const a = document.createElement("a"),
            url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);  
    }, 0); 
  }

  // Using `let` instead of `const` to allow for multiple executions.
  const messages = document.querySelectorAll('[data-testid=chat-message-text]');
  const dataItems = [];

  for (const message of messages) {
    dataItems.push(`${message.attributes['data-author'].value}: ${message.textContent}`);
  }

  saveFile(dataItems.join('\n'), `replika_${Date.now()}.txt`, 'text');

  console.log('Saved.');
})();
