function runSave(tab, saveHtml) {
  if (saveHtml) {
    chrome.scripting.executeScript({
      target: { tabId: tab.id },
      files: ['saveHtml.js']
    }); 
  } else {
    chrome.scripting.executeScript({
      target: { tabId: tab.id },
      files: ['save.js']
    }); 
  }
}

document.getElementById('saveText').addEventListener('click', async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
  runSave(tab);
});

document.getElementById('saveHtml').addEventListener('click', async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });
  runSave(tab, true);
});
