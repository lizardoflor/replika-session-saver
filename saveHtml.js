(function(){
  function saveFile(data, filename, type) {
    var file = new Blob([data], {type: type});
    const a = document.createElement("a"),
            url = URL.createObjectURL(file);
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    setTimeout(function() {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);  
    }, 0); 
  }

  const timestamp = Date.now();
  const messages = document.querySelectorAll('[data-testid=chat-message-text]');
  const dataItems = [];

  for (const message of messages) {
    dataItems.push(`<div class="message-div"><span class="message ${message.attributes['data-author'].value}">${message.textContent}</span></div>`);
  }

  const htmlTemplate = `<!DOCTYPE html>
  <html>
    <header>
      <title>Replika Session - ${timestamp}</title>
      <style type="text/css">
        body {
          margin:auto;
          max-width: 600px;
          background-color:  rgb(20, 16, 70);
          color: white;
          font-family: "Muli", -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif;
        }
        .content {
          min-width: 400px;
          margin: 20px;
        }
        .message-div {
          width: 100%;
          margin: 2px;
          display: inline-block;
        }
        .message {
          background-color: rgb(51, 56, 114);
          border-radius: 20px;

          padding-bottom: 11px;
          padding-left: 19px;
          padding-right: 19px;
          padding-top: 10px;
          display: inline-block;
          max-width: 350px;
          overflow-wrap: break-word;
          text-size-adjust: 100%;
          font-size: 16px;
          line-height: 20px;
        }
        .user {
          float: right;
        }
      </style>
    </header>
    <body>
      <div class="content">
        ${dataItems.join('\n')}
      </div>
    </body>
  </html>`;

  saveFile(htmlTemplate, `replika_${Date.now()}.html`, 'text/html');

  console.log('Saved.');
})();